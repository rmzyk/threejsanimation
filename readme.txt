Dzień dobry,

gdyby projekt nie działał poprawnie proszę zastosować 
następujące kroki.

1. Ściągnąć package manager (np. npm lub yarn)
2. Otworzyć command line w folderze z solucją i użyc komendy 'npm install'
3. Następnie w cmd wpisac 'npm run start'. 

Otworzy się domyślna przeglądarka z lokalnym serwerem, który poprawnie odtworzy projekt.

