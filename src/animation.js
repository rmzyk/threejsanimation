import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { ImageLoader } from "three";


//INIT
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
var renderer = new THREE.WebGLRenderer();
renderer.setClearColor( 0xFFFFFF);
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
camera.position.z = 3;

//RESIZE WINDOW FUNCTION
window.addEventListener('resize', 
() =>{
renderer.setSize(window.innerWidth, window.innerHeight);
camera.aspect = window.innerWidth/window.innerHeight;
camera.updateProjectionMatrix();
})


//ORBIT CONTROL
var controls = new OrbitControls( camera, renderer.domElement );
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.screenSpacePanning = false;
controls.enableZoom = false;

//TEXTURES
var worldGeometry = new THREE.BoxGeometry( 1000, 1000, 1000 );
var textures = [
	new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("miscellaneous/texture/skybox/xpos.png"), side: THREE.DoubleSide}),
	new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("miscellaneous/texture/skybox/xneg.png"), side: THREE.DoubleSide}),
	new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("miscellaneous/texture/skybox/ypos.png"), side: THREE.DoubleSide}),
	new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("miscellaneous/texture/skybox/yneg.png"), side: THREE.DoubleSide}),
	new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("miscellaneous/texture/skybox/zpos.png"), side: THREE.DoubleSide}),
	new THREE.MeshBasicMaterial( { map: new THREE.TextureLoader().load("miscellaneous/texture/skybox/zneg.png"), side: THREE.DoubleSide})
]

//WORLD CUBE

var worldMaterial = new THREE.MeshFaceMaterial(textures );
var world = new THREE.Mesh( worldGeometry, worldMaterial ); 

scene.add( world );

// PARTICLES

var particleGeometry = new THREE.SphereGeometry(1, 8, 8); // size, number of polys to form this circle
var particleMaterial = new THREE.MeshBasicMaterial({
	wireframe: true,
	color: 0x2857a3,
	transparent: true,
	opacity: 0.5
});


var particleCount = 20000;
var particles = [];
for(var i=0; i<particleCount; i++){
particles[i]= new THREE.Mesh(particleGeometry, particleMaterial);

var px = Math.random() * window.innerWidth * 2 - window.innerWidth;
var py = Math.random() * window.innerHeight * 2 - window.innerHeight;
var pz = Math.random() * window.innerWidth * 2 - window.innerWidth;

particles[i].position.x = px;
particles[i].position.y = py;
particles[i].position.z = pz;

particles[i].direction = {
	x: Math.random(),
	y: Math.random()
}
scene.add(particles[i]);
}


//AUDIO
var listener = new THREE.AudioListener();
camera.add( listener );

var sound = new THREE.Audio( listener );

var audioLoader = new THREE.AudioLoader();
audioLoader.load( 'miscellaneous/sound/bach.ogg', function( buffer ) {
	sound.setBuffer( buffer );
	sound.setLoop( true );
	sound.setVolume( 0.2 );
	sound.play();
});




//ANIMATE
var animate = () => {
requestAnimationFrame( animate );
for (var i = 0; i < particleCount; i++) {
	particles[i].position.x += particles[i].direction.x + 0.5;
	particles[i].position.y += particles[i].direction.y + 0.5;

	if (particles[i].position.x < -window.innerWidth ||
	particles[i].position.x > window.innerWidth) {
		particles[i].direction.x = -particles[i].direction.x;
	}
	if (particles[i].position.y < -window.innerHeight ||
	particles[i].position.y > window.innerHeight) {
		particles[i].direction.y = -particles[i].direction.y;
	}
}

controls.update()
renderer.render( scene, camera );
};

animate();